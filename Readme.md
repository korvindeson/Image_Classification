# Machine Learning Engineer Nanodegree
# Deep Learning
## Project: Classify imagen from CIFAR10 dataset.

### Install

This project requires **Python 3.5** with the tensotflow library installed.

### Code

Template code is provided in the `image_classification.ipynb` file.

